const { authenticate } = require('@feathersjs/authentication').hooks;
const fs = require('fs')

const addDefaultValues = function (config) {
  return async context => {
    const {template} = context.data;

    const templateContent =  fs.readFileSync(`${__dirname}/../../templates/${template}.html`).toString();
    context.data.userId = context.params.user._id;
    context.data.content = templateContent;
    return context;
  };
}

const validateInput = function(config){
  return async context => {
    const {template} = context.data;
    if(!template || template.trim().length === 0) {
      throw new Error('Template is required');
    }
    return context;
  };
}

const ownDataOnly = function(config){
  return async context => {
    context.params.query = {
      userId: context.params.user._id
    }
    return context;
  };
}

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [ownDataOnly()],
    get: [ownDataOnly()],
    create: [validateInput(), addDefaultValues()],
    update: [ownDataOnly()],
    patch: [ownDataOnly()],
    remove: [ownDataOnly()]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
